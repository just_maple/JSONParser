let str = `[11,{"tes":"asf" ,"asf":{"et":12412} },"242",{"tt":[1,[2,{"s":""}]],"etet":{"aa":[222,111],"fsf":{"wr":122,"22":2142}},"etetsas":[1324,15,"asf",false]}]`;
console.log(parseJSON(str));
console.log(JSON.parse(str));

function parseJSON(str) {
  let tokens = parseTokens(str);
  console.log(tokens);
  function isObj(tokens) {
    return tokens[0] === "{" && tokens[1] === '"' && tokens[3] === '"' && tokens[4] === ':'
  }


  function isNullStr(tokens) {
    return tokens[0] === '"' && tokens[1] === '"'
  }

  function isArray(tokens) {
    return tokens[0] === "["
  }

  function isStr(tokens) {
    return tokens[0] === '"' && tokens[2] === '"'
  }

  function isKey(tokens) {
    return tokens[0] === '"' && tokens[2] === '"' && tokens[3] === ":"
  }

  function isNewObj(tokens) {
    return isKey(tokens) && isObj(tokens.slice(4, tokens.length))
  }

  function parseIn(tokens) {
    let tks = tokens.slice(1, tokens.length);
    switch (tokens[0]) {
      case '{':
        return parse(tks);
      case '[':
        return parseArray(tks);
      default:
        return null
    }
  }

  function parseArray(tokens) {
    let array = [];
    let pointer = 0;
    while (pointer < tokens.length) {
      let tk = tokens[pointer];
      let subtks = tokens.slice(pointer, tokens.length);
      console.log(array, tk, subtks);
      switch (true) {
        case tk === '[': {
          let newNode = parseArray(subtks.slice(1, subtks.length));
          pointer += newNode.pointer + 1;
          array.push(newNode.node);
          break;
        }
        case tk === ']': {
          return {node: array, pointer: pointer + 1}
        }
        case tk === '{': {
          let newNode = parse(subtks.slice(1, subtks.length));
          pointer += newNode.pointer + 1;
          array.push(newNode.node);
          break;
        }
        case isNullStr(subtks):
          array.push("");
          break;
        case tk === ',':
          pointer += 1;
          break;
        case isStr(subtks):
          array.push(subtks[1]);
          pointer += 3;
          break;
        default:
          array.push(eval(tk));
          pointer += 1;
      }
    }
  }

  function parse(tokens) {
    let node = {};
    let key = "";
    let value = null;
    let pointer = 0;
    while (pointer < tokens.length) {
      let tk = tokens[pointer];
      let subtks = tokens.slice(pointer, tokens.length);
      console.log(subtks, node, tk);
      switch (true) {
        case isNewObj(subtks): {
          key = subtks[1];
          let newNode = parse(subtks.slice(5, subtks.length));
          console.log("get new obj", newNode);
          value = newNode.node;
          pointer += newNode.pointer + 5;
          break;
        }
        case tk === '{': {
          key = subtks[2];
          let newNode = parse(subtks.slice(5, subtks.length));
          console.log("get new obj", newNode);
          value = newNode.node;
          pointer += newNode.pointer + 5;
          break;
        }
        case tk === '[':
          let array = parseArray(subtks.slice(1, subtks.length));
          console.log("get array", array);
          value = array.node;
          pointer += array.pointer + 1;
          break;
        case isKey(subtks):
          key = subtks[1];
          pointer += 4;
          break;
        case isNullStr(subtks):
          value = '';
          pointer += 2;
          break;
        case tk === ',':
          pointer += 1;
          break;
        case isStr(subtks):
          value = subtks[1];
          pointer += 3;
          break;
        case tk === '}':
          return {node: node, pointer: pointer + 1};
          break;
        default:
          value = eval(tk);
          pointer += 1;
      }
      if (key !== '' && value !== null) {
        node[key] = value;
        key = "";
        value = null
      }
    }
    return {node: node}
  }

  return parseIn(tokens).node
}

function parseTokens(str) {
  let tks = [];
  let otherStack = "";
  let strFlag = false;
  let escapeFlag = false;
  for (let c of str) {
    function clearStack() {
      if (otherStack.length) {
        tks.push(otherStack);
      }
      otherStack = "";
      tks.push(c);
    }

    function pushTks() {
      if (escapeFlag) {
        otherStack += c;
        escapeFlag = false;
      } else if (strFlag) {
        if (c === '"') {
          strFlag = false;
          clearStack();
        } else {
          otherStack += c;
        }
      } else {
        clearStack();
        if (c === '"') {
          strFlag = true
        }
      }
    }

    switch (true) {
      case c === '\\':
        if (!escapeFlag) {
          escapeFlag = true;
        } else {
          pushTks();
        }
        break;
      case ['"', '{', ':', '}', ',', '[', ']'].indexOf(c) !== -1 :
        pushTks();
        break;
      default:
        otherStack += c
    }
  }
  return tks
}
